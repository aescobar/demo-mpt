package com.wtc.aee.milkrun.beans;

import com.wtc.aee.milkrun.dao.EmbalagenBL;
import com.wtc.aee.milkrun.entities.Embalagen;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Created by ariel on 3/10/18.
 */
@ManagedBean(name = "embalagenBean")
@ViewScoped
public class EmbalagenBean {

    @EJB
    EmbalagenBL embalagenBL;

    private Embalagen embalagenSelect;

    public Embalagen getEmbalagenSelect() {
        return embalagenSelect;
    }

    public void setEmbalagenSelect(Embalagen embalagenSelect) {
        this.embalagenSelect = embalagenSelect;
    }

    public void salvar(){

    }
}
