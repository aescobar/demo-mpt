package com.wtc.aee.milkrun.dao;

import com.wtc.aee.milkrun.entities.Embalagen;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by ariel on 3/10/18.
 */
@Stateless
public class EmbalagenBL {


    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("proto-mpt");
    private EntityManager em = factory.createEntityManager();

    public Embalagen getEmbalagen(Long id) {
        try {
            Embalagen user = (Embalagen) em.createQuery("SELECT u from Embalagen u where u.id = :id").setParameter("id", id).getSingleResult();
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean inserirEmbalagen(Embalagen user) {
        try {
            em.persist(user);
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean atualizarEmbalagen(Embalagen user) {
        try {
            em.merge(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean deletarEmbalagen(Embalagen user) {
        try {
            em.remove(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


}
