package com.wtc.aee.milkrun.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by ariel on 3/10/18.
 */
@Entity
@Table(name = "embalagen")
public abstract class Embalagen implements Serializable {

    @Id
    @GeneratedValue(generator = "embalagen_id")
    @SequenceGenerator(
            name = "embalagen_id",
            sequenceName = "embalagen_sequence",
            initialValue = 1000
    )
    private Long id;

    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "tipo", nullable = false)
    private String tipo;

    @Column(name = "comprimento")
    private BigDecimal comprimento;

    @Column(name = "largura")
    private BigDecimal largura;

    @Column(name = "altura")
    private BigDecimal altura;

    @Column(name = "peso")
    private BigDecimal peso;

    @Column(name = "imagen")
    private String imagen;

    public Embalagen() {
    }

    public Embalagen(String nome, String tipo, BigDecimal comprimento, BigDecimal largura, BigDecimal altura, BigDecimal peso, String imagen) {
        this.nome = nome;
        this.tipo = tipo;
        this.comprimento = comprimento;
        this.largura = largura;
        this.altura = altura;
        this.peso = peso;
        this.imagen = imagen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public BigDecimal getComprimento() {
        return comprimento;
    }

    public void setComprimento(BigDecimal comprimento) {
        this.comprimento = comprimento;
    }

    public BigDecimal getLargura() {
        return largura;
    }

    public void setLargura(BigDecimal largura) {
        this.largura = largura;
    }

    public BigDecimal getAltura() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    public BigDecimal getPeso() {
        return peso;
    }

    public void setPeso(BigDecimal peso) {
        this.peso = peso;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}